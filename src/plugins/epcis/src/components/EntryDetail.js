// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import React, {Component} from "react";
import {FormattedMessage} from "react-intl";
import {Card, Icon} from "@blueprintjs/core";
import {RightPanel} from "components/layouts/Panels";
import {connect} from "react-redux";
import {loadEntry, getGeoForEntry, loadEntryEvents, loadEntryByID} from "../reducers/epcis";
import {EventDetailTable} from "./EventDetailTable";
import {EventsTimeline} from "./EventsTimeline";
import {MapBody} from "./MapBody";
import "./EntryDetail.css";

class _EntryDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      geoEvents: [],
      currentEntryEvents: [],
      eventsIDList: [],
      latestDisposition: "",
      latestLotNo: ""
    };
  }

  componentDidMount() {
    this.setState({
      geoEvents: this.props.geoEvents ? this.props.geoEvents : []
    });
    this.props.loadEntry(this.props.server, this.props.match.params.entryID);
    this.props.getGeoForEntry(
      this.props.server,
      this.props.match.params.entryID
    );
    this.props.loadEntryByID(this.props.server,sessionStorage.getItem("EntryTopID"));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.geoEvents) {
      this.setState({geoEvents: nextProps.geoEvents});
    }
    if(nextProps.location.pathname !== this.props.location.pathname) {
      this.props.loadEntry(nextProps.server, nextProps.match.params.entryID);
      this.props.getGeoForEntry(
        nextProps.server,
        nextProps.match.params.entryID
      );
    }
    if(nextProps.currentEntryEvents !== undefined) {
      this.setState({
        currentEntryEvents: nextProps.currentEntryEvents
      })
    }

    let selectedObj=[];
    nextProps.currentEntryEvents
    ?
    nextProps.currentEntryEvents.events.map((item,index)=>(
      selectedObj.push(item[Object.keys(item)])
    ))
    :
    ""
    if(typeof selectedObj == 'object'){
      console.log(selectedObj);
      selectedObj.map((item,index) =>(
        loadEntryEvents(this.props.server, JSON.parse(JSON.stringify(item)).id)
        .then(raw=>{
          raw ?
          this.setState({
            eventsIDList: [...this.state.eventsIDList, Object.values(raw.reduce((acc,cur)=>Object.assign(acc,{[cur.id]:cur}),{}))[0]]
          })
          :
          "-"
        })
        .then(()=>{
          this.setState({
            eventsIDList: [...new Map(this.state.eventsIDList.map(item => [item.event_id, item])).values()],
            latestDisposition: selectedObj[selectedObj.length-1].disposition === null ? "-" : selectedObj[selectedObj.length-1].disposition ,
            latestLotNo: selectedObj[0].ilmd && selectedObj[0].action==="ADD" && selectedObj[0].bizStep.split(":")[selectedObj[0].bizStep.split(":").length-1]==="commissioning" ? selectedObj[0].ilmd.lotNumber : ""
          })
        })
      ));
    }
  }

  render() {
    const {server} = this.props;
    const {currentEntryEvents} = this.state;
    return (
      <RightPanel
        title={(
          <FormattedMessage
            id="plugins.epcis.entryDetail"
            defaultMessage="Entry Detail"
          />
)}
      >
        <div className="large-cards-container full-large">
          <Card>
            <h5>
              {this.props.match.params.entryID}
            </h5>
            {currentEntryEvents && currentEntryEvents.events ? (
              <div>
                <Card className="pt-elevation-4">
                  <div className="card-general-info">
                    <span>
                        <FormattedMessage
                          id="plugins.epcis.entryGeneralInfo"
                          defaultMessage="General informations:"
                        />
                    </span>
                  </div>
                  <table className="pt-table data-pair-table pt-bordered pt-striped">
                    <tbody>
                      <tr>
                        <td>
                          <FormattedMessage
                            id="plugins.epcis.entryLotnumber"
                            defaultMessage="Lotnumber:"
                          />
                        </td>
                        <td>{this.state.latestLotNo}</td>
                      </tr>
                      <tr>
                        <td>
                          <FormattedMessage
                            id="plugins.epcis.entryDisposition"
                            defaultMessage="Disposition:"
                          />
                        </td>
                        <td>
                          {this.state.latestDisposition}
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <FormattedMessage
                            id="plugins.epcis.topLevelParent"
                            defaultMessage="Top level aggregation parent:"
                          />
                        </td>
                        <td>
                          {this.props.entriesByID && sessionStorage.getItem("EntryTopID") !== null ? this.props.entriesByID.identifier : "-"}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </Card>
                <EventsTimeline events={currentEntryEvents.events} />
                {this.state.geoEvents.length > 0 ? (
                  <MapBody geoEvents={this.state.geoEvents} />
                ) : null}
                <br />
                {currentEntryEvents.events.map(event => {
                  return (
                    <EventDetailTable
                      className="entry-events-container"
                      currentEntry={event}
                      history={this.props.history}
                      serverID={server.serverID}
                      server={server}
                      eventsIDList={this.state.eventsIDList}
                    />
                  );
                })}
              </div>
            ) : null}
          </Card>
        </div>
      </RightPanel>
    );
  }
}

export const EntryDetail = connect(
  (state, ownProps) => {
    const hasProp = propName => {
      return (
        state.epcis.servers
        && state.epcis.servers[ownProps.match.params.serverID]
        && state.epcis.servers[ownProps.match.params.serverID][propName]
      );
    };
    return {
      server: state.serversettings.servers[ownProps.match.params.serverID],
      geoEvents: hasProp("geoEvents")
        ? state.epcis.servers[ownProps.match.params.serverID].geoEvents
        : [],
      currentEntryEvents: hasProp("detailItems")
        ? state.epcis.servers[ownProps.match.params.serverID].detailItems[
          ownProps.match.params.entryID
        ]
        : null,
      entriesByID: state.epcis.servers[ownProps.match.params.serverID].entriesByID,
    };
  },
  {loadEntry, getGeoForEntry, loadEntryByID}
)(_EntryDetail);
