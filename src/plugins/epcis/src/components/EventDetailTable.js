
// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
import React, {Component} from "react";
import {FormattedMessage} from "react-intl";
import {Card,Icon} from "@blueprintjs/core";
import What from "./EventDetailTable/What";
import When from "./EventDetailTable/When";
import Where from "./EventDetailTable/Where";
import Why from "./EventDetailTable/Why";
export class EventDetailTable extends Component {
  getObjectType = currentEntry => {
    try {
      let key = Object.keys(currentEntry);
      return key[0];
    } catch (e) {
      return null;
    }
  };
  goTo = path => {
    this.props.history.push(path);
  };
  
  render() {
    const {currentEntry, serverID} = this.props;
    let objectType = this.getObjectType(currentEntry);
    let isReady = () => {
      return currentEntry && currentEntry[objectType];
    };
    return (
      <div id={isReady() ? currentEntry[objectType].id : null}>
        {isReady() ? (
          <div className={this.props.className} >
            
              <span className="event-information-separator" xyz="">
              <Card>
                <div>
                <table className="pt-table pt-bordered pt-striped">
                  <tbody>
                    <tr>
                      <div className="card-general-info">
                      <FormattedMessage
                        id="plugins.epcis.eventInfo"
                        defaultMessage="Event Informations"
                      />
                      </div>
                    </tr>
                    <tr>
                      <td>
                        <span className="event-detail-type">
                          <FormattedMessage
                            id="plugins.epcis.eventName"
                            defaultMessage="Event Name:"
                          />
                        </span>
                      </td>
                      {
                      this.props.eventsIDList 
                      &&
                      this.props.eventsIDList.length > 0
                      &&
                      this.props.eventsIDList.map((item, index) => ( 
                      item.event_id == currentEntry[objectType].id
                      ?
                        <td >
                          {
                          item.task_name === null 
                          ?  
                          "Task Name not available"
                          : 
                          <span className="event-detail-taskID">
                            {item.task_name}
                            <a
                            style={{color: "#FFF", paddingLeft: "10px",paddingRight: "10px"}}
                            href={`${this.props.server.url}capture/task-data/${item.task_name}/`}
                            target="_blank"
                            >
                              <button class="pt-button pt-interactive pt-intent-primary pt-btn-flex">
                                <span>
                                  Download
                                </span>
                                <Icon
                                    iconName="pt-icon-cloud-download"
                                  />
                              </button>
                            </a>
                          </span>
                          }
                        </td>
                      :
                      "" 
                      ))
                    }
                    </tr>
                    
                    { 
                    this.props.isEventDetail
                    ?
                    <tr>
                      <td>
                        <span className="event-detail-type">
                          <FormattedMessage
                            id="plugins.epcis.entryLotnumber"
                            defaultMessage="Lotnumber:"
                          />
                        </span>
                      </td>
                      {
                    this.props.isEventDetail
                    &&
                    this.props.latestLotNo !== undefined
                    &&
                    this.props.latestLotNo.length > 0
                    ?  
                    <td >
                      <span className="event-detail-taskID">
                        {this.props.latestLotNo[0].objectEvent ?
                        this.props.latestLotNo[0].objectEvent.ilmd.lotNumber
                        :
                        "-"}
                      </span>
                    </td>
                    :
                    ""
                    }
                    </tr> 
                    :
                    ""  
                    }
                   
                  </tbody>
                </table> 
                </div>
              </Card>
              </span>
              
            
            <Card>
              <What
                currentEntry={currentEntry}
                objectType={objectType}
                goTo={this.goTo.bind(this)}
                serverID={serverID}
              />
            </Card>
            <Card>
              <When
                currentEntry={currentEntry}
                objectType={objectType}
                goTo={this.goTo.bind(this)}
                serverID={serverID}
              />
            </Card>
            <Card>
              <Where
                currentEntry={currentEntry}
                objectType={objectType}
                goTo={this.goTo.bind(this)}
                serverID={serverID}
              />
            </Card>
            <Card>
              <Why
                currentEntry={currentEntry}
                objectType={objectType}
                goTo={this.goTo.bind(this)}
                serverID={serverID}
              />
            </Card>
          </div>
        ) : null}
      </div>
    );
  }
}
