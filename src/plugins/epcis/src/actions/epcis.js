import {createAction} from "redux-actions";

export default {
  loadEntries: createAction("EPCIS_LOAD_ENTRIES"),
  loadEvents: createAction("EPCIS_LOAD_EVENTS"),
  loadEventsByIlmd: createAction("EPCIS_LOAD_EVENTS_BY_ILMD"),
  loadEventsByBizTransaction: createAction("EPCIS_LOAD_EVENTS_BY_BIZ_TRANSACTION"),
  loadEntryEvents: createAction("EPCIS_LOAD_ENTRY_EVENTS"),
  loadItemDetail: createAction("EPCIS_LOAD_ITEM_DETAIL"),
  loadGeoEvents: createAction("EPCIS_LOAD_GEO_EVENTS"),
  clearGeoEvents: createAction("CLEAR_GEO_EVENTS"),
  loadEntryByID: createAction("EPCIS_LOAD_ENTRY_BY_ID"),
  loadEventsByEntryID: createAction("EPCIS_LOAD_EVENT_BY_ENTRY_ID"),
  clearEventsByEntryID: createAction("CLEAR_EPCIS_LOAD_EVENT_BY_ENTRY_ID")
};
