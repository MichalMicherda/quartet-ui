## ![QU4RTET-UI](./icon.png "Title Text") QU4RTET-UI NEW FEATURE
### Description
(Describe suggestions for new feature)
### Place for new feature
(Exact place where you would like to add new feature)
(example: Clicked A-> Clicked B -> Opened A -> Moved back to A)
### What is the expected new feature behavior?
{+ (What you should see) +}
### Mockups
(If you can, please add a mockup)

/label ~new_feature
