## ![QU4RTET-UI](./icon.png "Title Text") QU4RTET-UI BUG REPORT

### Version

(Number of QU4RTET-UI Version)

### Summary

(Summarize the bug encountered concisely)

### Steps to reproduce

(How one can reproduce the issue - this is very important)
(example: Clicked A-> Clicked B -> Opened A -> Moved back to A)

### What is the current bug behavior?
{- (What actually happens) -}
### What is the expected correct behavior?
{+ (What you should see instead) +}
### Relevant logs and/or screenshots

(Paste any relevant logs - use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

### Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug ~needs-investigation
