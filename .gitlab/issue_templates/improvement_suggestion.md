## ![QU4RTET-UI](./icon.png "Title Text") QU4RTET-UI IMPROVEMENT SUGGESTION
### Description
(Describe suggestions for improvement)
### Place for improvement
(Exact place where you would like to add improvement)
(example: Clicked A-> Clicked B -> Opened A -> Moved back to A)
### What is the current application behavior?
{- (What actually happens) -}
### What is the expected application behavior?
{+ (What you should see instead) +}
### Mockups
(If you can, please add a mockup)

/label ~improvement
